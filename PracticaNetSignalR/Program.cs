using Microsoft.EntityFrameworkCore;
using PracticaNetSignalR.Hubs;
using PracticaNetSignalR.Models;

var builder = WebApplication.CreateBuilder(args);

var provider = builder.Services.BuildServiceProvider();
var configuration = provider.GetService<IConfiguration>();
var connection = configuration.GetValue<string>("ConnectionStrings:DefaultConnection");


// Add services to the container.
builder.Services.AddControllersWithViews();

// Agregar el Servicio de SignalR
builder.Services.AddSignalR();

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
});
builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseSqlServer(connection));
var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

//Agregar url para acceder a los Hub
app.MapHub<ProjectHub>("/projectHub");
app.MapHub<ChatHub>("/chat");
app.MapHub<ChartHub>("/chart");
app.Run();
