﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PracticaNetSignalR.Models
{
    public class Project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Título")]
        public string Title { get; set; }

        [Required]
        [DisplayName("Descripción")]
        public string Description { get; set; }

        [DisplayName("Url Documentación")]
        public string? Url { get; set; }

        [DisplayName("Link Repositorio")]
        public string? Repository { get; set; }
    }
}
