﻿using System.ComponentModel;

namespace PracticaNetSignalR.Models
{
    public class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
