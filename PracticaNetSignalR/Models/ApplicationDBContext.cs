﻿using Microsoft.EntityFrameworkCore;

namespace PracticaNetSignalR.Models
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }

        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<Room> Room { get; set; }

        public virtual DbSet<Programming> Programming { get; set; }
    }
}
