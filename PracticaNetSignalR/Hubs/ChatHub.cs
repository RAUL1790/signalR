﻿using Microsoft.AspNetCore.SignalR;

namespace PracticaNetSignalR.Hubs
{
    public class ChatHub: Hub
    {
    
    
        public async Task SendMessageRoom(string room, string user, string message)
        {           
            await Clients.Group(room).SendAsync("ReceiveMessageRoom",user, message, DateTime.Now.ToString("hh:mm tt"));
        }

        public async Task AddToGroup(string room)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, room);
            await Clients.Group(room).SendAsync("ShowWho",Context.ConnectionId);
        }


    }
}
