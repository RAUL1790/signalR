﻿using Microsoft.AspNetCore.SignalR;

namespace PracticaNetSignalR.Hubs
{
    public class ChartHub : Hub
    {
        public async Task Vote(string vote)
        {
            await Clients.All.SendAsync("ReceiveVote", vote);
        }
    }
}
