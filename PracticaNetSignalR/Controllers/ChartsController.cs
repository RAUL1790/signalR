﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PracticaNetSignalR.Models;

namespace PracticaNetSignalR.Controllers
{
    public class ChartsController : Controller
    {

        private readonly ApplicationDBContext _context;


        public ChartsController(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            List<object> data = new List<object>();        
            var lista = _context.Programming.ToList();
            foreach (var item in lista)
            {
                object[] obj = new object[2];
                obj[0] = item.Name;
                obj[1] = 0;
                data.Add(obj);
            }
            ViewBag.data = data;
            return View(lista);
        }

        public async Task<IActionResult> Room(int id)
        {
            var room = _context.Room.FindAsync(id).Result;
            return View(room);
        }
    }
}
