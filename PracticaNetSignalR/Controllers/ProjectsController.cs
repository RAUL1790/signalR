﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using PracticaNetSignalR.Hubs;
using PracticaNetSignalR.Models;

namespace PracticaNetSignalR.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly ApplicationDBContext _context;
        private readonly IHubContext<ProjectHub> _projectHub;

        public ProjectsController(ApplicationDBContext context, IHubContext<ProjectHub> projectHub)
        {
            _context = context;
            _projectHub = projectHub;
        }


        public async Task<IActionResult> Index()
        {
            return _context.Project != null ?
                        View(await _context.Project.ToListAsync()) :
                        Problem("Entity set 'ApplicationDBContext.Project'  is null.");
        }

        [HttpGet]
        public IActionResult GetProjects()
        {
            var res = _context.Project.ToList();
            return Ok(res);
        }

       
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Project == null)
            {
                return NotFound();
            }

            var Project = await _context.Project
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Project == null)
            {
                return NotFound();
            }

            return View(Project);
        }

       
        public IActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Url,Repository")] Project Project)
        {
            if (ModelState.IsValid)
            {
                _context.Add(Project);
                await _context.SaveChangesAsync();
                await _projectHub.Clients.All.SendAsync("LoadProyects");
                return RedirectToAction(nameof(Index));
            }
            return View(Project);
        }

      
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Project == null)
            {
                return NotFound();
            }

            var Project = await _context.Project.FindAsync(id);
            if (Project == null)
            {
                return NotFound();
            }
            return View(Project);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,Url,Repository")] Project Project)
        {
            if (id != Project.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(Project);
                    await _context.SaveChangesAsync();
                    await _projectHub.Clients.All.SendAsync("LoadProyects");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectExists(Project.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(Project);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Project == null)
            {
                return NotFound();
            }

            var Project = await _context.Project
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Project == null)
            {
                return NotFound();
            }

            return View(Project);
        }

      
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Project == null)
            {
                return Problem("Entity set 'ApplicationDBContext.Project'  is null.");
            }
            var Project = await _context.Project.FindAsync(id);
            if (Project != null)
            {
                _context.Project.Remove(Project);
            }
            
            await _context.SaveChangesAsync();
            await _projectHub.Clients.All.SendAsync("LoadProyects");
            return RedirectToAction(nameof(Index));
        }

        private bool ProjectExists(int id)
        {
          return (_context.Project?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
