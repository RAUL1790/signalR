﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using PracticaNetSignalR.Hubs;
using PracticaNetSignalR.Models;

namespace PracticaNetSignalR.Controllers
{
    public class ChatsController : Controller
    {

        private readonly ApplicationDBContext _context;


        public ChatsController(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            return _context.Room != null ?
                        View(await _context.Room.ToListAsync()) :
                        Problem("Entity set 'ApplicationDBContext.Room'  is null.");
        }

        public async Task<IActionResult> Room(int id)
        {
            var room = _context.Room.FindAsync(id).Result; 
            return View(room);
        }
    }
}
