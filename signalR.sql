USE [master]
GO
/****** Object:  Database [signalR]    Script Date: 25/8/2022 16:38:05 ******/
CREATE DATABASE [signalR]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'signalR', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\signalR.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'signalR_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\signalR_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [signalR] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [signalR].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [signalR] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [signalR] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [signalR] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [signalR] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [signalR] SET ARITHABORT OFF 
GO
ALTER DATABASE [signalR] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [signalR] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [signalR] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [signalR] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [signalR] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [signalR] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [signalR] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [signalR] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [signalR] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [signalR] SET  DISABLE_BROKER 
GO
ALTER DATABASE [signalR] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [signalR] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [signalR] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [signalR] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [signalR] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [signalR] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [signalR] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [signalR] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [signalR] SET  MULTI_USER 
GO
ALTER DATABASE [signalR] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [signalR] SET DB_CHAINING OFF 
GO
ALTER DATABASE [signalR] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [signalR] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [signalR] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [signalR] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'signalR', N'ON'
GO
ALTER DATABASE [signalR] SET QUERY_STORE = OFF
GO
USE [signalR]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 25/8/2022 16:38:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Programming]    Script Date: 25/8/2022 16:38:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Programming](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Programming] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Project]    Script Date: 25/8/2022 16:38:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Url] [nvarchar](max) NULL,
	[Repository] [nvarchar](max) NULL,
 CONSTRAINT [PK_Tema] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 25/8/2022 16:38:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220817202735_Initial', N'6.0.8')
GO
SET IDENTITY_INSERT [dbo].[Programming] ON 

INSERT [dbo].[Programming] ([Id], [Name]) VALUES (1, N'C#')
INSERT [dbo].[Programming] ([Id], [Name]) VALUES (2, N'Java')
INSERT [dbo].[Programming] ([Id], [Name]) VALUES (3, N'Python')
INSERT [dbo].[Programming] ([Id], [Name]) VALUES (4, N'PHP')
SET IDENTITY_INSERT [dbo].[Programming] OFF
GO
SET IDENTITY_INSERT [dbo].[Project] ON 

INSERT [dbo].[Project] ([Id], [Title], [Description], [Url], [Repository]) VALUES (2, N'BlazoryElectron', N'Ejemplo de integración Blazon y Electrón', N'https://www.c-sharpcorner.com/blogs/create-desktop-application-using-blazor-electron', N'https://github.com/electron/electron')
INSERT [dbo].[Project] ([Id], [Title], [Description], [Url], [Repository]) VALUES (4, N'Api .Net 6', N'Ejemplo de Api en .Net 6', N'https://docs.microsoft.com/en-us/aspnet/core/tutorials/min-web-api?view=aspnetcore-6.0&tabs=visual-studio', N'https://github.com/orgs/dotnet/projects/20')
INSERT [dbo].[Project] ([Id], [Title], [Description], [Url], [Repository]) VALUES (5, N'Inyección de Dependencias', N'Ejemplo de Inyección de Dependencias', N'https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0', N'https://github.com/rcaram22/Clase-5-Inyeccion-de-Dependencias')
SET IDENTITY_INSERT [dbo].[Project] OFF
GO
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([Id], [Name]) VALUES (1, N'Práctica .Net')
INSERT [dbo].[Room] ([Id], [Name]) VALUES (2, N'Officina')
INSERT [dbo].[Room] ([Id], [Name]) VALUES (3, N'MSC')
INSERT [dbo].[Room] ([Id], [Name]) VALUES (4, N'YPF')
INSERT [dbo].[Room] ([Id], [Name]) VALUES (5, N'Programación')
SET IDENTITY_INSERT [dbo].[Room] OFF
GO
USE [master]
GO
ALTER DATABASE [signalR] SET  READ_WRITE 
GO
